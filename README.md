# Qantas Code Challenge

Design and create a simple web crawler. It needs to take a URL as a parameter and create a tree of child pages linked to the URL. It�s expected that your application provides deep-crawling solution, meaning that it goes through multiple levels in link hierarchy.

## Getting Started

Unzip the contents of the supplied .zip file and run mvn spring-boot:run

### Prerequisites

In order to run this application please make sure you have java 8 and maven installed. I am running version 3.5.0


## Running the tests

The end to end tests can be run via the mvn verify command.
or by loading up the project into your favourite ide.  Note some of the tests will take time and also log
HttpStatusExceptions. This is expected behaviour, the tests traverse links found on pages, it is expected
that some links will not be accessible due to security i.e. authorization exceptions, some links maybe broken
etc.

The expected output should be

Tests run: 5, Failures: 0, Errors: 0, Skipped: 0

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 03:53 min
[INFO] Finished at: 2017-09-10T12:13:35+10:00
[INFO] Final Memory: 36M/277M
[INFO] ------------------------------------------------------------------------

Add additional notes about how to deploy this on a live system

## Built With

* [AngularJS 1.5.5](https://angular.io/) - Client side JS Library
* [Angular Block UI](http://angular-block-ui.nullest.com) - Block UI Angular JS Library
* [Angular Tree Control](https://github.com/wix/angular-tree-control) - Client side Angular Tree JS Library
* [Bootstrap 3.3.6](https://angular.io/) - Bootstrap CSS
* [Maven](https://maven.apache.org/) - Dependency Management
* [Spring Boot 1.5.2.RELEASE](https://projects.spring.io/spring-boot/)
* [Lombok 1.16.18](https://projectlombok.org/)  
* [JSoup 1.9.1](https://jsoup.org/) - HTML parsing
* [Swagger 2.2.2](https://swagger.io/) - RESTFull end point documentation tool
* [ehcache](http://www.ehcache.org/) - Cache used to store traversed URL'S

## Angular Client Documentation
- the Angular Client can be accessed via
http://localhost:8080/qantas-code-challenge/
- directions on how to use the client are listed on the main page.
- The client represents the pages scraped in an Asynchronous Tree. 
Each child node expansion will return only those child nodes associated to the parent.
- The client is the best way to test the web crawlers deep page traversal, it has no
limitations as it always retrieves 1 level of child nodes and adds the returned nodes to the tree.

## Swagger Documentation
The swagger ui can be accessed via 

http://localhost:8080/qantas-code-challenge/swagger-ui.html
selecting the web-crawler-resource : link will display the available end points.
Swagger will allow the user to test the individual end points.

Each end point submits a POST with the following JSON object as the body

````
{
  "allowExternalSiteCrawling": true,
  "depth":0,
  "url": "",
  "baseUrl":""
}

setting the allowExternalSiteCrawling to false will result
in the external links to not be displayed nor crawled.

depth represents how deep the crawler will crawl. Note for the sake
of this coding challenge the max depth has been set to 3. Anything larger
could result in a very length process plus a huge JSON dump. Setting depth levels
of less than 3 will result in requested depth. Depths set to greater than 3 will be capped
at max depth. If the value is unspecified it will be set to a depth of 1.

url represents the root url of the page the web crawler will start at.
baseUrl is auto populated when using the tree client, it is required due to the fact
that we are only retrieving 1 level at a time and thus need to know the original starting point.

````



## Authors

* **Alex Shkolyar** - *Initial work* - [ashkolyar@yahoo.com](ashkolyar@yahoo.com)


