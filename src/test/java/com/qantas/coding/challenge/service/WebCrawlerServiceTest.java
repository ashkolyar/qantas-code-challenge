package com.qantas.coding.challenge.service;

import com.qantas.coding.challenge.model.Page;
import net.sf.ehcache.CacheManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

/**
 * Created by alexshkolyar on 9/9/17.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableCaching
public class WebCrawlerServiceTest {
    @Autowired
    private WebCrawlerService webCrawlerService;
    @Autowired
    private CacheManager cacheManager;

    private static final String CACHE_NAME = "urlCache";
    private static final String PAGE_CACHE = "pageCache";

    @Before
    public void clearCache() {
        cacheManager.getCache(CACHE_NAME).removeAll();
        cacheManager.getCache(PAGE_CACHE).removeAll();
    }

    @Test
    public void testThatThePageObjectReturnedHasNoExternalLinks() throws URISyntaxException {

        String url = "https://jsoup.org/";
        int maxDepth = 1;

        Page root = webCrawlerService.getPage(url, maxDepth, false,"127.0.0.1");

        assertNotNull(root);
        String domain = "jsoup.org";

        assertTrue(doesNotContainExternalLink(root, domain));
    }

    @Test
    public void testThatThePageObjectReturnedHasExternalLinks() throws URISyntaxException {
        String url = "https://jsoup.org/";

        int maxDepth = 1;

        Page root = webCrawlerService.getPage(url, maxDepth, true,"127.0.0.1");

        assertNotNull(root);
        String domain = "jsoup.org";

        assertFalse(doesNotContainExternalLink(root, domain));
    }

    @Test
    public void testThatARootNodeWithNoChildrenIsReturned() throws URISyntaxException {
        String url = "https://jsoup.org/";

        Page root = webCrawlerService.getRootNode(url, false, "https://jsoup.org/","127.0.0.1");

        assertNotNull(root);
        assertEquals(0, root.getNodes().size());
        assertTrue(!root.isLeaf());

    }

    @Test
    public void testThatTopLevelNodesAreReturnedForAGivenUrl() throws URISyntaxException {
        String url = "https://jsoup.org/";

        List<Page> nodes = webCrawlerService.getNodes(url, false, "https://jsoup.org/","127.0.0.1");

        assertNotNull(nodes);
        for (Page node : nodes) {
            assertEquals(0, node.getNodes().size());
        }

    }

    @Test(expected = IllegalArgumentException.class)
    public void testThatIllegalArgumentExceptionIsThrownForInvalidURI() throws URISyntaxException {
        String url = "blah://adfldjfhdslj.com/";
        webCrawlerService.getPage(url, 3, false, "127.0.0.1");
    }

    private boolean doesNotContainExternalLink(Page node, String domain) {
        return getFlattenedPageList(node).filter(urlLink -> !urlLink.getUrl().
                contains(domain)).collect(toList()).isEmpty();
    }

    private Stream<Page> getFlattenedPageList(Page page) {
        return Stream.concat(
                Stream.of(page),
                page.getNodes().stream().flatMap(this::getFlattenedPageList));
    }
}