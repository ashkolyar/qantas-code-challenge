(function() {
    var app;

    app = angular.module('qantas-app', ['blockUI','ngAnimate','treeControl', 'ui.bootstrap']);

    app.controller('qantasCtrl', function($scope, $http, $timeout, blockUI) {
        $scope.pages = [];

        $scope.url = '';
        $scope.depth = 1;
        $scope.externalCrawling = false;

        $scope.loadingTime = 1500;
        $scope.treeModel = [];

        $scope.treeOptions = {
            dirSelectable: false,    // Click a folder name to expand (not select)
            isLeaf: function isLeafFn(node) {
                return node.leaf || node.visited;
            }
        };

        $scope.fetchChildNodes = function fetchChildNodes(node, expanded) {
            function doFetch(node) {
                if (!node.leaf && !node.visited) {
                    blockUI.start();

                     var requestOption = {
                        url : node.url,
                        baseUrl: $scope.url,
                        depth: 1,
                        allowExternalSiteCrawling:$scope.externalCrawling
                    };

                    var res = $http.post('/qantas-code-challenge/api/webcrawler/fetch/nodes', requestOption);
                    res.success(function(data, status, headers, config) {
                        node.children = data;
                        blockUI.stop();
                    });
                    res.error(function(data, status, headers, config) {
                        blockUI.stop();
                        if(status === 500){
                            alert("An unexpected error hass occurred unable to process your request");
                        } else {
                            alert("failure message: " + JSON.stringify({data: data}));
                        }
                    });
                }
            }

            if (!node._sent_request) {
                 node._sent_request = true;
                 doFetch(node);
            }
        };

        
        $scope.fetchNodes = function(){
            $scope.treeModel = [];
            blockUI.start();
            var requestOption = {
                url : $scope.url,
                baseUrl: $scope.url,
                depth: 1,
                allowExternalSiteCrawling:$scope.externalCrawling
            };

            var res = $http.post('/qantas-code-challenge/api/webcrawler/fetch/node', requestOption);
            res.success(function(data, status, headers, config) {
                $scope.treeModel.push(data);
                blockUI.stop();
            });
            res.error(function(data, status, headers, config) {
                blockUI.stop();

                if(status === 500){
                    alert("An unexpected error hass occurred unable to process your request");
                } else {
                    alert("failure message: " + JSON.stringify({data: data}));
                }

            });

        };
    });

}).call();
