package com.qantas.coding.challenge.service;

import com.qantas.coding.challenge.model.CacheKey;
import com.qantas.coding.challenge.model.Page;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by alexshkolyar on 9/9/17.
 */

@Service
public class WebCrawlerServiceImpl implements WebCrawlerService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CacheManager cacheManager;

    /**
     * returns the {@code com.qantas.coding.challenge.model.Page} root node of the crawled hierarchy
     *
     * @param url
     * @param depth
     * @param allowExternalSiteCrawling
     * @param requestIP
     * @return
     * @throws URISyntaxException
     */
    @Override
    public Page getPage(String url, Integer depth, boolean allowExternalSiteCrawling, String requestIP) throws URISyntaxException {
        Page root;

        CacheKey key = CacheKey.builder().url(url).requestIP(requestIP).build();

        Cache pageCache = this.cacheManager.getCache(PAGE_CACHE);

        Cache.ValueWrapper obj = pageCache.get(key);

        root = obj != null ? (Page) obj.get() : null;

        //if the root is not null then just return the page for efficiency
        if (root == null) {

            String domain = this.getDomainName(url);

            if (depth > MAX_DEPTH) {
                depth = MAX_DEPTH;
            }

            try {
                Document doc = Jsoup.connect(url).timeout(CONNECTION_TIMEOUT).get();

                String title = doc.title();

                root = Page.builder().title(title).url(url).nodes(new ArrayList<>()).build();

                this.cacheManager.getCache(CACHE_NAME).put(key, root);

                root = getPagesForUrl(root, doc, domain, depth, 0, allowExternalSiteCrawling, requestIP);

                //store the page object against the root url for efficiency
                pageCache.put(key, root);
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        }

        return root;
    }

    /**
     * returns the top level {@code com.qantas.coding.challenge.model.Page} root node with no children
     *
     * @param url
     * @param allowExternalSiteCrawling
     * @param baseUrl
     * @param requestIP
     * @return
     * @throws URISyntaxException
     */
    @Override
    @SuppressWarnings("unchecked")
    public Page getRootNode(String url, boolean allowExternalSiteCrawling, String baseUrl, String requestIP) throws URISyntaxException {
        Page root = null;

        String domain = this.getDomainName(url);

        CacheKey key = CacheKey.builder().requestIP(requestIP).url(url).baseUrl(baseUrl).build();

        try {

            net.sf.ehcache.Cache cache = (net.sf.ehcache.Cache) this.cacheManager.
                    getCache(WEB_CLIENT_CACHE).getNativeCache();

            //if we have an existing entry for this key
            //i.e. requestIP baseUrl then we need to remove all entries associated with this key
            //the key contains the requestiP and baseurl as there could be multiple requests
            //from different clients.
            if(cache.get(key) != null){
               cache.getKeys().stream().
                       filter(k -> key.getBaseUrl().
                               equals(((CacheKey)k).getBaseUrl()) &&
                               key.getRequestIP().equals(((CacheKey)k).getRequestIP())).forEach(cache::remove);
            }

            //get the root node
            Document doc = Jsoup.connect(url).timeout(CONNECTION_TIMEOUT).get();

            root = Page.builder().title(doc.title()).url(url).nodes(new ArrayList<>()).build();

            //determine if it has links
            setLeaf(allowExternalSiteCrawling, root, domain, doc);
            this.cacheManager.getCache(WEB_CLIENT_CACHE).put(key, root);

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return root;
    }


    /**
     * returns a list o {@code com.qantas.coding.challenge.model.Page} representing the children for the supplied root node
     *
     * @param url
     * @param allowExternalSiteCrawling
     * @param baseUrl
     * @param requestIP
     * @return
     * @throws URISyntaxException
     */

    @Override
    public List<Page> getNodes(String url, boolean allowExternalSiteCrawling, String baseUrl, String requestIP) throws URISyntaxException {
        List<Page> pages = new ArrayList<>();

        String domain = this.getDomainName(url);

        try {
            //get the root node
            Document doc = Jsoup.connect(url).timeout(CONNECTION_TIMEOUT).get();

            Elements links = doc.select(CSS_SELECTOR);
            links.forEach(link -> addPage(allowExternalSiteCrawling, pages, domain, link, requestIP, baseUrl));

        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }

        return pages;
    }

    private void addPage(boolean allowExternalSiteCrawling, List<Page> pages,
                         String domain,
                         Element link,
                         String requestIP,
                         String baseUrl) {
        Cache webClientCache = this.cacheManager.getCache(WEB_CLIENT_CACHE);

        String urlLink = link.attr(CSS_ATTRIBUTE);


        CacheKey key = CacheKey.builder().requestIP(requestIP).url(urlLink).baseUrl(baseUrl).build();

        Cache.ValueWrapper obj = webClientCache.get(key);

        Page child = obj != null ? (Page) obj.get() : null;

        if (child == null) {
            try {
                boolean proceed = true;

                if (!allowExternalSiteCrawling && !urlLink.contains(domain)) {
                    proceed = false;
                }

                if (proceed) {
                    Document childDoc = Jsoup.connect(urlLink).timeout(CONNECTION_TIMEOUT).get();
                    child = Page.builder().title(childDoc.title()).url(urlLink).nodes(new ArrayList<>()).build();

                    webClientCache.put(key, child);

                    this.setLeaf(allowExternalSiteCrawling, child, domain, childDoc);
                    pages.add(child);
                }
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            //if its in cache no need to re-visit just add it to the list of pages
            Page visited = Page.builder().
                    title(child.getTitle()).
                    url(child.getUrl()).
                    visited(true).
                    leaf(child.isLeaf()).
                    nodes(new ArrayList<>()).build();

            pages.add(visited);
        }
    }

    /**
     * recursive function to get all page nodes upto a specified depth
     *
     * @param node
     * @param doc
     * @param domain
     * @param maxDepth
     * @param currentDepth
     * @param allowExternalSiteCrawling
     * @param requestIP
     * @return
     */
    private Page getPagesForUrl(Page node,
                                Document doc,
                                String domain,
                                int maxDepth,
                                int currentDepth,
                                boolean allowExternalSiteCrawling,
                                String requestIP) {


        currentDepth += 1;

        //get all links and recursively call the processPage method
        Elements links = doc.select(CSS_SELECTOR);
        if (currentDepth <= maxDepth) {

            for (Element link : links) {
                String urlLink = link.attr(CSS_ATTRIBUTE);
                if (!StringUtils.isEmpty(urlLink)) {

                    CacheKey key = CacheKey.builder().requestIP(requestIP).url(urlLink).build();

                    Cache.ValueWrapper obj = this.cacheManager.getCache(CACHE_NAME).get(key);

                    Page child = obj != null ? (Page) obj.get() : null;

                    if (child == null) {
                        //only visit the page if the url is not in the cache as we don't want circular reference
                        try {

                            boolean proceed = true;

                            if (!allowExternalSiteCrawling && !urlLink.contains(domain)) {
                                proceed = false;
                            }

                            if (proceed) {
                                Document childDoc = Jsoup.connect(urlLink).timeout(CONNECTION_TIMEOUT).get();

                                child = Page.builder().title(childDoc.title()).url(urlLink).nodes(new ArrayList<>()).build();

                                this.cacheManager.getCache(CACHE_NAME).put(key, child);

                                getPagesForUrl(child, childDoc, domain, maxDepth, currentDepth, allowExternalSiteCrawling, requestIP);

                                node.setLeaf(false);

                                node.getNodes().add(child);
                            }
                        } catch (IOException e) {
                            if (e instanceof UnsupportedMimeTypeException) {
                                logger.debug(e.getMessage() + " url : " + urlLink + " not visited");
                            } else {
                                logger.error(e.getMessage(), e);
                            }
                        }
                    } else {
                        Page visited = Page.builder().title(child.getTitle()).
                                url(child.getUrl()).
                                visited(true).leaf(child.isLeaf()).
                                nodes(new ArrayList<>()).build();

                        node.getNodes().add(visited);
                    }
                }
            }
        }

        return node;
    }

    private String getDomainName(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    private void setLeaf(boolean allowExternalSiteCrawling, Page root, String domain, Document doc) {
        Elements links = doc.select(CSS_SELECTOR);

        if (!links.isEmpty()) {
            if (allowExternalSiteCrawling) {
                root.setLeaf(false);
            } else {
                List<String> filteredLinks = links.stream().map(link -> link.attr(CSS_ATTRIBUTE)).filter(urlLink -> !StringUtils.isEmpty(urlLink) && urlLink.contains(domain)).collect(toList());
                root.setLeaf(filteredLinks.isEmpty());
            }
        }
    }
}
