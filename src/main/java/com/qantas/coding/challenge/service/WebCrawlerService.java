package com.qantas.coding.challenge.service;

import com.qantas.coding.challenge.model.Page;

import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by alexshkolyar on 9/9/17.
 */
public interface WebCrawlerService {
    /**
     * if no depth is specified then make sure that the crawler doesn't exceed
     * a depth of 3 as the JSON structure will be too large.
     */
    int MAX_DEPTH = 3;
    int CONNECTION_TIMEOUT = 1500;
    String CSS_SELECTOR = "a[href]";
    String CSS_ATTRIBUTE = "abs:href";
    String CACHE_NAME = "urlCache";
    String PAGE_CACHE = "pageCache";
    String WEB_CLIENT_CACHE = "webClientCache";

    /**
     * returns the {@code com.qantas.coding.challenge.model.Page} root node of the crawled hierarchy
     *
     * @param url
     * @param depth
     * @param allowExternalSiteCrawling
     * @param requestIP
     * @return
     * @throws URISyntaxException
     */
    Page getPage(String url, Integer depth, boolean allowExternalSiteCrawling, String requestIP) throws URISyntaxException;


    /**
     * returns the top level {@code com.qantas.coding.challenge.model.Page} root node with no children
     *
     * @param url
     * @param allowExternalSiteCrawling
     * @param baseUrl
     * @param requestIP
     * @return
     * @throws URISyntaxException
     */
    Page getRootNode(String url, boolean allowExternalSiteCrawling, String baseUrl, String requestIP) throws URISyntaxException;

    /**
     * returns a list o {@code com.qantas.coding.challenge.model.Page} representing the children for the supplied root node
     *
     * @param url
     * @param allowExternalSiteCrawling
     * @param baseUrl
     * @param requestIP
     * @return
     * @throws URISyntaxException
     */
    List<Page> getNodes(String url, boolean allowExternalSiteCrawling, String baseUrl, String requestIP) throws URISyntaxException;
}
