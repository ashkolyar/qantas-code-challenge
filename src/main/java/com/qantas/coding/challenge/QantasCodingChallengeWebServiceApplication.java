package com.qantas.coding.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by alexshkolyar on 9/9/17.
 */

@SpringBootApplication
@EnableAutoConfiguration
@EnableCaching
@EnableSwagger2
public class QantasCodingChallengeWebServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(QantasCodingChallengeWebServiceApplication.class, args);
    }

    @Bean
    public Docket qantasCodeChallengeSwaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Qantas Code Challenge with Swagger")
                .description("Qantas Code Challenge with Swagger")
                .contact("Alex Shkolyar")
                .build();
    }
}
