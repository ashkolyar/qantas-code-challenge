package com.qantas.coding.challenge.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by alexshkolyar on 9/9/17.
 * A model object used to represent a tree structure of pages visited
 */

@Data
@Builder
public class Page implements Serializable {
    private String url;
    private String title;
    private List<Page> nodes;
    private boolean leaf = true;
    private boolean visited;
}
