package com.qantas.coding.challenge.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Created by alexshkolyar on 9/9/17.
 * A model object representing the data passed in a POST request from the client
 */
@Data
public class RequestOption {
    @ApiModelProperty(notes = "The url used by the web crawler to fetch and traverse")
    private String url;
    //used for web client as we are always returning one level we
    //need to know what the original url was.
    @ApiModelProperty(notes = "The base url is only used when called from the client side tree and is auto populated")
    private String baseUrl;

    @ApiModelProperty(notes = "The max depth the web crawler should traverse default set to 1")
    //default value in case a value isn't specified
    private int depth = 1;

    @ApiModelProperty(notes = "Boolean flag to allow/disallow external site crawling")
    private boolean allowExternalSiteCrawling;
}
