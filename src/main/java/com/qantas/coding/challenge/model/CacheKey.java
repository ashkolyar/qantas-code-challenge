package com.qantas.coding.challenge.model;

import lombok.Builder;
import lombok.Data;

/**
 * Created by alexshkolyar on 17/9/17.
 */
@Data
@Builder
public class CacheKey {
    private String requestIP;
    private String url;
    private String baseUrl;
}
