package com.qantas.coding.challenge.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by alexshkolyar on 9/9/17.
 * A model object used by a ui-client to display top level nodes
 */

@Data
@Builder
public class TreeNode implements Serializable {
    private String url;
    private String name;
    private boolean leaf = true;
    private boolean visited;
    private List<TreeNode> children;
}
