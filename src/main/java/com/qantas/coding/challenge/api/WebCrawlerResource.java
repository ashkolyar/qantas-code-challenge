package com.qantas.coding.challenge.api;

import com.qantas.coding.challenge.model.Page;
import com.qantas.coding.challenge.model.RequestOption;
import com.qantas.coding.challenge.model.TreeNode;
import com.qantas.coding.challenge.service.WebCrawlerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexshkolyar on 9/9/17.
 */
@RestController
@RequestMapping("api/webcrawler")
public class WebCrawlerResource {

    @Autowired
    WebCrawlerService webCrawlerService;


    @ApiOperation(value = "fetchTree", nickname = "fetchTree")
    @RequestMapping(value = "/fetch", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = TreeNode.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public Page getTree(@RequestBody RequestOption requestOption, HttpServletRequest request) {
        try {
            String requestIP = request.getRemoteAddr();

            return webCrawlerService.getPage(requestOption.getUrl(),
                    requestOption.getDepth(),
                    requestOption.isAllowExternalSiteCrawling(), requestIP);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

    @ApiOperation(value = "fetchRoot", nickname = "fetchRootNode")
    @RequestMapping(value = "/fetch/node", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = TreeNode.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public TreeNode getRootNodeForTree(@RequestBody RequestOption requestOption, HttpServletRequest request) {
        try {

            String requestIP = request.getRemoteAddr();

            Page root = webCrawlerService.getRootNode(requestOption.getUrl(),
                    requestOption.isAllowExternalSiteCrawling(), requestOption.getBaseUrl(), requestIP);

            return TreeNode.builder().
                    leaf(root.isLeaf()).
                    name(root.getUrl()).
                    url(root.getUrl()).
                    children(new ArrayList<>()).
                    build();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return null;
    }

    @ApiOperation(value = "fetchNodes", nickname = "fetchChildNodes")
    @RequestMapping(value = "/fetch/nodes", method = RequestMethod.POST)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = TreeNode.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    public List<TreeNode> getChildNodesForTreeView(@RequestBody RequestOption requestOption, HttpServletRequest request) {
        try {

            String requestIP = request.getRemoteAddr();

            List<Page> nodes = webCrawlerService.getNodes(requestOption.getUrl(),
                    requestOption.isAllowExternalSiteCrawling(), requestOption.getBaseUrl(), requestIP);

            List<TreeNode> treeNodes = new ArrayList<>();
            nodes.forEach(p -> treeNodes.add(TreeNode.builder().
                    leaf(p.isLeaf()).
                    name(p.getUrl()).
                    url(p.getUrl()).
                    visited(p.isVisited()).
                    children(new ArrayList<>()).
                    build()));

            return treeNodes;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
